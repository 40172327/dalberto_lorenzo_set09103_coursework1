from flask import Flask, render_template, url_for, redirect, json, request
app = Flask(__name__)





# main route of the application that thesplay the home page
@app.route('/')
def munrohome():
  return render_template('/home-munro.html'), 200

#addmunro route where user will enter details of a munro
@app.route('/add-munro.html', methods=['GET', 'POST'])
def addmunro():
  if request.method == "POST":
    #Open mnList.txt and load from it the list of munros into mnList
    with open('mnList.txt', 'r') as jsonFile:
      mnList = json.load(jsonFile)

    #Create a munro dictionary
      munro = {'name':request.form['mnName'],
             'desc':request.form['mnDesc'],
             'region':request.form['mnRegion'],
             'height':request.form['mnHeight'],
             'date':request.form['mnDate'],
             'image':request.form['mnImage']}

    #A munro is added to the list
      mnList.append(munro)

    #The list is rewritten with the munro added to it
    with open('mnList.txt', 'w') as file:
      file.write(json.dumps(mnList))
    #if a "POST" is detected the page add-munro.html will be displayed
    return render_template('add-munro.html', munro=munro)
  else:
    return render_template('add-munro.html')

#route displaying the list of munros
@app.route('/list-munros.html', methods=['GET'])
def listmunros():

  #the text file containing the list of munros is opend and loaded into a list
  #in json format
  with open('mnList.txt', 'r') as jsonFile:
    mnList = json.load(jsonFile)

    #Attemted to introduce a delete function but failed due to lack of
    #time
    #name = request.form['mnToDisplay']
    #print name


    #newList=[munro for munro in mnList if munro['name'] !=
    #request.form['mnDelete']]

    #mnList = newList

    #with open('mnList.txt', 'w') as file:
     #file.write(json.dumps(mnList))

    #with open('mnList.txt', 'r') as jsonFile:
     # mnNewList = json.load(jsonFile)

    #the list is returned in the template list-munros.html
    return render_template('list-munros.html', mnList=mnList)

#route handling an error in case user types wrong address for the page
@app.errorhandler(404)
def page_not_found(error):
  return render_template('404.html')

#@app.route('/')
#def homemunro():
  #return render_template('/home-munro.html')


#route to display an indiviadua munro
@app.route('/display-munro/', methods=['GET'])
def displaymunro():
  #the text file containing the list of munro is read and loaded into a list as
  #injson format
  with open('mnList.txt', 'r') as jsonFile:
    mnList = json.load(jsonFile)

    
    #the name of the munro is stored in the variable mnToDisplay
    #and used to identify the munro to display
    mnToDisplay=request.args.get('mnToDisplay')
    #to identify the munro to display a new list is created with only the munro
    #we are interested in
    mnFilteredList = [munro for munro in mnList if
          munro['name'] == mnToDisplay]
    #mnDisplay will take the first element of the list that we wat to display
    mnDisplay = mnFilteredList[0]

    #the details of the munro selected are the passed to the template
    #display-munro.html
    return render_template('display-munro.html', mnDisplay=mnDisplay)


if __name__ == "__main__":
  app.run(host='0.0.0.0', debug=True)

